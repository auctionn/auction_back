import json
import pickle
from ast import literal_eval
from datetime import datetime, date
from pprint import pprint

from elasticsearch import Elasticsearch
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
# ES_URL = 'https://search-courtauction-57zi33dfghf5skgln34rtumndm.ap-northeast-2.es.amazonaws.com'
# from common_util.const_value import ES_URL
from common_util.const_value import ES_URL # ide는 Server가 현재 디렉토리, 그러나 실행은 manage를 기준으로 경로를 설정해야함

# from common_util.util import getDomain, isRefer, PutFilter, isInQuery, SetFlag, DoFlag
from common_util.util import isRefer
from home.functions import PutFilter, isInQuery, SetFlag, DoFlag
from home.functions import GetSpecial, InitSpecial, GetProcedure, GetStreet


es = Elasticsearch(ES_URL);


def index(request) :

    print("-------------Home Start------------------")
    # print(request)
    # print("Request        :  %s" %request.GET)
    req_content = request.GET;
    print("Request.sw_lat :  %s" %request.GET.get('sw_lat'))
    print("Request.ne_lat :  %s" %request.GET.get('ne_lat'))
    print("Request.sw_lng :  %s" %request.GET.get('sw_lng'))
    print("Request.ne_lng :  %s" %request.GET.get('ne_lng'))
    print("Order By       :  %s" %request.GET.get('orderby'))
    print("House Query    :  %s" %request.GET.get('hq'))
    print("Data Filter    :  %s" %request.GET.get('date'))
    print("App Price      :  %s" %request.GET.get('app'))
    print("Min Price      :  %s" %request.GET.get('min'))
    print("Yuchal Price   :  %s" %request.GET.get('mis'))
    print("Add            :  %s" %request.GET.get('add'))
    print("Num            :  %s" %request.GET.get('num'))
    print("Query          :  %s" %request.GET.get('query'))
    print("Level          :  %s" %request.POST.get('level'))
    print("Lan            :  %s" %request.POST.get('Lan'))
    print("Lnt            :  %s" %request.POST.get('Lnt'))
    print("POST            :  %s" %request.POST)

    sw_lat = req_content.get('sw_lat')
    sw_lng = req_content.get('sw_lng')
    ne_lat = req_content.get('ne_lat')
    ne_lng = req_content.get('ne_lng')
    center_lat = 37.49
    center_lng = 127.02
    zoom = 6
    currentPage = req_content.get('currentPage')
    recordPerPage = req_content.get('$recordPerPage ')
    orderby = req_content.get('orderby')
    hq = req_content.get('hq')
    keyword = req_content.get('keyword')
    search_query = req_content.get('query') if req_content.get('query') else ''
    min_platArea = 0
    max_platArea = 101300
    min_archArea = 0
    max_archArea = 101300
    street_num = req_content.get('num') if req_content.get('num') else ''
    special_querys = [0]
    special_filter = request.GET.get('add') if request.GET.get('add') is not None else ''
    special_filter = special_filter.split(',')


    # --------------Flag 설정 -------------------------
    #     0: 초기화, 1: 필터없음, 2: 필터있음
    date_list = request.GET.get('date').split(',')
    date_list = SetFlag(date_list,'1999.01.01','2200.01.01')
    from_date = date_list[0]
    to_date = date_list[1]
    date_flag = date_list[2]

    app_price_list = request.GET.get('app').split(',')
    app_price_list = SetFlag(app_price_list, -1, 1882810000000)
    from_app_price = int(app_price_list[0])
    to_app_price = int(app_price_list[1])
    app_flag = app_price_list[2]

    min_price_list = request.GET.get('min').split(',')
    min_price_list = SetFlag(min_price_list, -1, 1882810000000)
    from_min_price = int(min_price_list[0])
    to_min_price = int(min_price_list[1])
    min_flag = min_price_list[2]

    yuchal_list = request.GET.get('mis').split(',')
    yuchal_list = SetFlag(yuchal_list, 0, 10000)
    from_yuchal = int(yuchal_list[0])
    to_yuchal = int(yuchal_list[1])
    yuchal_flag = yuchal_list[2]

    platArea_list = request.GET.get('plot').split(',') # 대지면적
    platArea_list = SetFlag(platArea_list, 0, 10000)
    from_platArea = int(platArea_list[0])
    to_platArea= int(platArea_list[1])
    platArea_flag = platArea_list[2]

    archArea_list = request.GET.get('area').split(',') # 건물면적
    archArea_list = SetFlag(archArea_list, 0, 10000)
    from_archArea= int(archArea_list[0])
    to_archArea= int(archArea_list[1])
    archArea_flag = archArea_list[2]


    court_y = "SaGeonBeonHo.y";
    court_type = "SaGeonNaeYeok.MulGeonNaeYeok.MokRokGuBun";
    court_type_value = "집합건물"

    arr = list()

    parsing_data = dict() # 최종적으로 보낼 데이터 딕셔너리

    if currentPage is None:
        currentPage = '1'
    if recordPerPage is None :
        recordPerPage = '24' # 현재 페이지 수

    print("This page : %s"%currentPage)
    print(recordPerPage)
    offset = (int(currentPage)-1) * int(recordPerPage)
    print(offset)

    # -------------------------------물건 정렬------------------------------
    order_target = req_content.get('orderby') # 정렬기준
    # if order_target == '' :
    #     order_target = 'non-select' # 정렬기준

    order = 'desc'
    if order_target == 'non-select' :
        order_target = court_y # Default는 위도의 내림차순
    if order_target == '' :
        pass;
    if order_target == 'disposal-date-desc':
        order_target  = 'SaGeonNaeYeok.MulGeonNaeYeok.GilJeonBo'
        order = 'desc'
    elif order_target == 'disposal-date-asc':
        order_target = 'SaGeonNaeYeok.MulGeonNaeYeok.GilJeonBo'
        order ='asc'
    elif order_target == 'case-number-desc':
        order_target = 'Sort_id'
        order = 'desc'
    elif order_target == 'case-number-asc':
        order_target = 'Sort_id'
        order = 'asc'
    elif order_target == 'appraise-desc':
        order_target = 'Sort_appraisal_price'
        order = 'desc'
    elif order_target == 'appraise-asc':
        order_target = 'Sort_appraisal_price'
        order = 'asc'
    elif order_target == 'min-desc':
        order_target = 'Sort_min_price'
        order = 'desc'
    elif order_target == 'min-asc':
        order_target = 'Sort_min_price'
        order = 'asc'
    elif order_target == 'h':
        order_target = 'SaGeonBeonHo.SoJaeJi'
        order = 'desc'
    elif order_target == 'i':
        order_target = 'SaGeonBeonHo.SoJaeJi'
        order = 'asc'
    elif order_target == 'miscarriage-desc':
        order_target = 'NakChalJeonBo.yuChal'
        order = 'desc'
    elif order_target == 'miscarriage-asc':
        order_target = 'NakChalJeonBo.yuChal'
        order = 'desc'

    print("order_target : %s order : %s" %(order_target,order))

    # ------------------------------------------------------------------

    # -------------------------------주거 목적별 필터------------------------------

    hq = req_content.get('hq')
    ex_hq = hq.split(",")
    match_hq = ''
    house_filed =''
    msq = ''
    if hq != '':
        ex_hq = hq.split(",")
        hq = ''
        for i in range(0 ,len(ex_hq)):
            if(ex_hq[i] == 'apt') :
                hq += ' 아파트'
            elif(ex_hq[i] == 'housing'):
                hq += ' 단독주택'
            elif (ex_hq[i] == 'neighborhoodhouse'):
                hq += ' 연립주택'
            elif (ex_hq[i] == 'room'):
                hq += ' 다세대'
            elif (ex_hq[i] == 'villa'):
                hq += ' 빌라'
            elif (ex_hq[i] == 'mall'):
                hq += ' 상가'
            elif (ex_hq[i] == 'facility'):
                hq += ' 근린시설'
            elif (ex_hq[i] == 'officetel'):
                hq += ' 오피스텔*'
            elif (ex_hq[i] == 'office'):
                hq += ' 사무실'
            else:
                hq += ' 없'

    if hq == '':
        match_hq = 'match_all'
        house_filed = ''
        match_hq_value = {'boost' : 0}

    else :
        match_hq = 'regexp'
        house_filed = 'SaGeonNaeYeok.MulGeonNaeYeok.MulGeonYongDo'
        hq = hq.replace(" ", "(", 1)
        hq = hq.replace(" ", "|")
        hq = hq + ")"
        match_hq_value = {house_filed: {'value': hq,  'boost': 1}}
    print("House Query : %s" %match_hq_value)
    # -------------------------------물건현황 필터처리 ------------------------------
    mulgeon_query = req_content.get('msq')
    # print("mq : %s" %mulgeon_query)
    if mulgeon_query is not None :
        ex_msq = mulgeon_query.split(',')
        for i in range(0 ,len(ex_msq)):
            print("ex msq : %s" % ex_msq[i])
            if (ex_msq[i] == 'progress'):
                msq += ' 공고'
            elif (ex_msq[i] == 'new'):
                msq += ' 신건'
            elif (ex_msq[i] == 'miscarriage'):
                msq += ' 유찰'
            elif (ex_msq[i] == 'disposal'):
                msq += ' 매각$'
            elif (ex_msq[i] == 'permission'):
                msq += ' 매각허가'
            elif (ex_msq[i] == 'paybalance'):
                msq += ' 납부'
            elif (ex_msq[i] == 'distribution'):
                msq += ' 배당기일종결'
            elif (ex_msq[i] == 'etcprogress'):
                msq += ' 진행 외 물건'
            elif (ex_msq[i] == 'nonprogress'):
                msq += ' 미진행'
            elif (ex_msq[i] == 'change'):
                msq += ' 변경'
            elif (ex_msq[i] == 'nonpermission'):
                msq += ' 불허가'
            elif (ex_msq[i] == 'stop'):
                msq += ' 정지'
            elif (ex_msq[i] == 'closeobject'):
                msq += ' 종국'
            elif (ex_msq[i] == 'dismissal'):
                msq += ' 각하'
            elif (ex_msq[i] == 'reject'):
                msq += ' 기각'
            elif (ex_msq[i] == 'transfer'):
                msq += ' 이송'
            elif (ex_msq[i] == 'cancel'):
                msq += ' 취소'
            elif (ex_msq[i] == 'drop'):
                msq += ' 취하'
            elif (ex_msq[i] == 'terminate'):
                msq += ' 모사건'

    else:
        msq = ''
    mulgeon_type = 'SaGeonNaeYeok.MulGeonNaeYeok.MulGeonSangTae'
    if msq != '':
        msq = msq.replace(" ", "(", 1)
        msq = msq.replace(" ", "|")
        msq = msq + ")"
    print(msq)

    # Deafault
    if search_query is '' :
        order_target = court_y
        query_body= {
            "from": offset,
            "size": recordPerPage,
            "sort": [
                {
                    order_target: {
                        "order": order
                    }
                }
            ],
            "query": {
                "bool": {
                    "must": [
                        {"range": {"SaGeonBeonHo.x": {"from": sw_lng, "to": ne_lng}}
                         },
                        {"range": {"SaGeonBeonHo.y": {"from": sw_lat, "to": ne_lat}}
                         },
                        {"range": {"SaGeonNaeYeok.MulGeonNaeYeok.GilJeonBo":
                                       {"from": from_date, "to": to_date}}
                         },
                        # {"range": {"building_archArea":
                        #                {"from": min_archArea, "to": max_archArea}}
                        #  },
                        # {"range": {"building_archArea":
                        #                {"from": min_platArea, "to": max_platArea}}
                        #  },
                        {"range": {"Sort_appraisal_price":
                                       {"from": from_app_price, "to": to_app_price}}
                         },
                        {"range": {"Sort_min_price":
                                       {"gte": from_min_price, "lte": to_min_price}}
                         },
                        {"range": {"NakChalJeonBo.yuChal":
                                       {"from": from_yuchal, "to": to_yuchal}}
                         },
                        {"match": {
                            court_type : court_type_value
                        }},
                        {match_hq: match_hq_value},
                        {"regexp":
                             {mulgeon_type:
                                  "[가-힣]*"+msq+"[가-힣]*"}
                         }
                    ]
                }
            }
        }
        # pprint(query_body)
    else:
        # -----------------------작업중------------
        query_body = literal_eval(search_query)
        query_body['from'] = offset
        query_body['size'] = recordPerPage
        match_oper = 'must'
        if 'should' in query_body['query']['bool']:
            #기본 13, 각 쿼리 필터 4,
            query_body['min_score'] = 13 + 4 + 1
            match_oper = 'should'
        else :
            query_body['min_score'] = 0

        min_price_query = {"range": {"Sort_min_price": {"from": from_min_price, "to": to_min_price, "boost": 1.0}}}
        app_price_query = {"range": {"Sort_appraisal_price": {"from": from_app_price, "to": to_app_price, "boost": 1.0}}}
        yuchal_query =  {"range": {"NakChalJeonBo.yuChal":{"from": from_yuchal, "to": to_yuchal, "boost": 1.0}}}
        mulgeon_query = {"regexp": {mulgeon_type: {"value" : "[가-힣]*" + msq + "[가-힣]*","boost": 1.0}}}
        date_query = {"range": {"SaGeonNaeYeok.MulGeonNaeYeok.GilJeonBo": {"from": from_date, "to": to_date, "boost": 1.0}}}
        house_query = {match_hq: match_hq_value}
        # platArea_query = {"range": {"building_platArea": {"from": from_platArea, "to": to_platArea, "boost": 1.0}}}
        # archArea_query = {"range": {"building_archArea": {"from": from_archArea, "to": to_archArea},"boost": 1.0}}

        DoFlag(min_flag, query_body, min_price_query)
        DoFlag(app_flag, query_body, app_price_query)
        DoFlag(yuchal_flag, query_body, yuchal_query)
        DoFlag(date_flag, query_body, date_query)
        # DoFlag(platArea_flag, query_body, platArea_query)
        # DoFlag(archArea_flag, query_body, archArea_query)


        PutFilter(query_body, mulgeon_query)
        if match_hq == 'match':
            query_body['min_score'] += 1
        PutFilter(query_body, house_query)
        query_body['min_score'] -= 0.1
        if order_target != '':
            query_body['sort'] = [{order_target: {"order": order}}]

        pprint("Before Query : %s" %query_body)
        # -------------------지번 범위---------------------------
        # print("------?")
        # print(street_num)
        # 현재 쿼리에 지번범위 쿼리가 있는지 확인
        check_index = -1
        s_type = 'regexp'
        s_field = 'SaGeonBeonHo.SoJaeJi'
        for index, item in enumerate(query_body['query']['bool'][match_oper]):
            item_type = list(item.keys())[0]
            item_field = list(item[item_type].keys())[0]
            if (item_type == s_type) & (item_field == s_field):
                print("in : %s  / %s"%(item_type, item_field))
                if 'boost' not in item[item_type][item_field]:
                    check_index = index
        print(check_index)
        if (street_num != '') & (street_num != ',,'):
            s_query = GetStreet(street_num.split(','))
            print(s_query)
            query_body['min_score'] += 1
            if check_index != -1:
                print("?????")
                query_body['query']['bool'][match_oper][check_index] = s_query
            else:
                query_body['query']['bool'][match_oper].append(s_query)
        else:
            if check_index != -1:
                query_body['min_score'] += 1
                if street_num == ',,':
                    query_body['query']['bool'][match_oper].pop(check_index)


        pprint("ABefore Query : %s" %query_body)
        # -------------------특수 물건---------------------------
        if len(special_filter) > 8:
            InitSpecial(query_body)
        else:
            special_querys = GetSpecial(special_filter)
        print("SQ : %s" %special_querys)
        query_body['min_score'] += float(special_querys[-1])
        special_querys.pop()

        for item in special_querys:
            item_key = list(item.keys())
            print(item_key)
            if item_key[0] == 'filter':
                query_body['query']['bool'].update(item)
            else:
                PutFilter(query_body,item)

        #------------------- 경매 절차 ----------------------------
        p_type = ''
        if 'all' in special_filter:
            p_type = 'all'
        elif 'random' in special_filter:
            p_type = 'random'
        elif 'force' in special_filter:
            p_type = 'force'

        procedure_query = GetProcedure(p_type)
        if procedure_query is not None:
            index = isInQuery(query_body, procedure_query)
            query_body['min_score'] += 1
            if index > -1 :
                query_body['query']['bool'][match_oper][index] = procedure_query
            else :
                PutFilter(query_body,procedure_query)
        else:
            temp = {
                "regexp" :{
                    "SaGeonNaeYeok.SaGeonGiBonNaeYeok.SaGeonMyeong" : {}
                }
            }
            if isInQuery(query_body,temp) > -1:
                query_body['min_score'] += 1
        print("-----------------------------------------------------------------")
        pprint("After Query : %s" %query_body)

    # data = es.search(index="court_en_backup2", body=query_body)
    data = es.search(index="court_en", body=query_body)
    data_hits = data['hits']['hits']


    # ------------ parsing data set -----------------------------
    parsing_data['foundRecord'] = data['hits']['total']
    parsing_data['currentPage'] = currentPage
    parsing_data['keyword'] = keyword

    print("Total : %s" %parsing_data['foundRecord'])
    count = 0;

    for v in data_hits :
        #----------- 정렬을 위한 사건번호 추출 --------------
        sort_id  = v['_id'].split('-')
        year = int(sort_id[0]) * 1000000
        sageon_id = int(sort_id[1])
        sort_id = int(year + sageon_id)

        # --------- 대표이미지 추출 ---------------------------
        if 'MulGeonSanSeJoHui' in  v['_source']:
            sajin = v['_source']['MulGeonSanSeJoHui']['MuGunSaJin']['SaJinJeongBo']
            if(len(sajin) > 0) :
                sajin = v['_source']['MulGeonSanSeJoHui']['MuGunSaJin']['SaJinJeongBo'][0]
            else :
                sajin = ''

            if sajin != '':
                ex_sajin = sajin.split("jpg?")
                sajin = ex_sajin[0]+'jpg'
        else:
            sajin = ''


        # ------- 감정평가액, 최저가격 추출-----------------------
        # print(v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok'])
        price = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['GamJeongPyeongGaAek']
        price = price.replace("원","")
        price = price.split(" ")
        # print("-------------------------------------------")
        if(len(price)  > 2) :
            appraisal_price = price[0]
            appraisal_price_min = price[2]
            price[2] = price[2].replace(",","")
            price[0] = price[0].replace(",","")
            appraisal_percent = int (int(price[2])*100 / int(price[0]))
        else :
            sort_price = ''
            sort_min_price = int(price[0].replace(",",""))
            appraisal_price = price[0]
            appraisal_price_min = ''
            appraisal_percent = ''


        # ------- 기일 D-day 추출-----------------------
        if 'GilJeonBo'in v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']:
            gildata = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['GilJeonBo']
            d_gildata = gildata.split('.')

            if len(d_gildata) > 2:
                targetDate = datetime.strptime(gildata,"%Y.%m.%d").date()
                toDate = date.today()
                d_day = toDate - targetDate
                d_day = d_day.days
            else:
                d_day = ''
        else :
            d_day = ''
            gildata = ''

        # ------- 물건상태 추출-----------------------
        status = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['MulGeonSangTae']
        status = status.replace(" ", "")
        status_arr = status.split("-")

        if len(status_arr) <= 1 :
            status = status_arr
        else :
            status = status_arr[len(status_arr) - 1]

        if "Sort_appraisal_price" not in v['_source']:
            v['_source']['Sort_appraisal_price'] =''

        if "Sort_min_price" not in v['_source']:
            v['_source']['Sort_min_price'] = ''

        if "Sort_min_price" not in v['_source']:
            v['_source']['Sort_min_price'] = ''

        record = dict()
        record['ID'] = v['_id']
        record['sort_id'] = v['_source']['Sort_id']
        record['record_id'] = v['_source']['SaGeonNaeYeok']['SaGeonGiBonNaeYeok']['SaGeonBunHo']
        record['record_title'] = record['record_id']
        record['record_type'] = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['MulGeonYongDo']
        record['record_address'] = v['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0]['SoJaeJi']  # 왜 0번째 내역을 가져오지?
        record['record_thumb'] = sajin
        record['record_gamjeongayaek'] = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['GamJeongPyeongGaAek']
        record['record_gil'] = v['_source']['GilNaeYeok']['GilNaeYeok']
        record['record_price'] = price
        record['record_Appraisal_price'] = appraisal_price
        record['sort_appraisal_price'] = v['_source']['Sort_appraisal_price']
        record['record_Appraisal_min_price'] = appraisal_price_min
        record['sort_min_price'] = v['_source']['Sort_min_price']
        record['record_Appraisal_price_percent'] = appraisal_percent
        record['gildata'] = gildata
        record['record_gil_dday'] = d_day
        record['lat'] = v['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0]['y']
        record['lng'] = v['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0]['x']
        record['record_MulGeonStatus'] = status

        #-----------Debug---------------
        record['score'] = v['_score']
        record['yuChal'] = v['_source']['NakChalJeonBo']['yuChal']
        record['GuBun'] = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['MokRokGuBun']
        if 'building_archArea' in v['_source']:
            record['Area'] = v['_source']['building_archArea']
        else :
            record['Area'] = -1
        record['rst'] = v['_source']['GilNaeYeok']['GilNaeYeok']
        record['Gday'] = v['_source']['SaGeonNaeYeok']['SaGeonGiBonNaeYeok']['GaeSiGyeolJyeonIlJa']
        record['Procedure'] = v['_source']['SaGeonNaeYeok']['SaGeonGiBonNaeYeok']['SaGeonMyeong']
        arr.append(record)

    # ------------Display--------------
    # for v in arr :
    #     print("-------------------------------------------")
    #     print("ID           : %s" %v['ID'])
    #     print("Gday         : %s" %v['Gday'])
        # print("RST          : %s" %v['rst'])
    #     print("Lat         : %s" %v['lat'])
    #     print("Ing         : %s" %v['lng'])
    #     print("SortPirce    :%s" %v['sort_appraisal_price'])
        # print("Sort     : %s" %v['sort_appraisal_price'])
        # print("RecordPrice: %s" %v['record_Appraisal_price'])
        # print("MinPrice     :%s" %v['sort_min_price'])
        # print("Sort ID    : %s" %v['sort_id'])
        # print("Date         : %s" %v['gildata'])
        # print("Area        : %s" %v['Area'])
        # print("Address  : %s" %v['record_address'])
        # print("Type     : %s" %v['record_MulGeonStatus'])
        # print("Score    : %s" %v['score'])
        # print("YuChal   : %s" %v['yuChal'])
        # print("GuBun      : %s" %v['GuBun'])
        # print("Procedure   : %s" %record['Procedure'])


    parsing_data['records'] = arr
    # print("Data : %s" %data_hits)
    parsing_data['query'] = query_body
    state_query = query_body;


    d = isRefer(request.get_host())

    if d['status'] == 'true':
        rst = {"status": d['status'], "message": d['message'], "data": parsing_data, "query": query_body, }
    else :
        http_host = request.get_host()
        request_uri = request.get_full_path()
        url = 'http://' + http_host + request_uri
        rst = {"status": d['status'], "message": d['message'], "data": parsing_data, "query": query_body, "url": url}

    rst = json.dumps(rst)

    # Cross Domain 설정
    response = HttpResponse(rst)
    response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    return response;






