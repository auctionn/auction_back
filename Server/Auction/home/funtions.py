import datetime

#------------ 필터 flag값을 설정해주고, 필터범위를 리턴합니다. -------------
def SetFlag(fill_list, initStart, initEnd):

    if fill_list[0] == '-1': # 초기화
        fill_list = [initStart,initEnd,1]
    elif fill_list[0] == '' : #필터없음
        fill_list = [initStart,initEnd,0]
    else :
        fill_list.append(1)
    return fill_list;

#------------ body 안에 query가 있는지 확인합니다.--------------------
def isInQuery(body,query) :
    match_body = body['query']['bool']
    match_oper = 'must'
    if 'should' in match_body:
        match_oper = 'should'
    match_body = match_body[match_oper]

    query_tpye = list(query.keys());
    query_tpye = query_tpye[0];
    inflag = -1;

    if query_tpye != 'match_all':
        query_field = list(query[query_tpye].keys());
        query_field = query_field[0];
        # print("Type  : %s" %query_tpye)
        # print("Field : %s" %query_field)
        for index, item in enumerate(match_body):
            # print("-------------------------")
            item_type = list(item.keys())[0]
            if item_type == query_tpye:
                item_field = list(item[item_type].keys())[0];
                # print("item field  : %s" %item_field)
                # print("query field : %s" %query_field)
                if item_field == query_field:
                    inflag = index;
        return inflag

#------------프론트에서 넘어온 쿼리에 필터쿼리를 추가합니다.-----------------
def PutFilter(body, query) :
    #     print("Parameter must be dict type");
    match_body = body['query']['bool']
    match_oper = 'must'
    if 'should' in match_body :
        match_oper = 'should'
    match_body = match_body[match_oper]

    query_tpye = list(query.keys());
    query_tpye = query_tpye[0];
    inflag = 0;

    if query_tpye != 'match_all' :
        query_field = list(query[query_tpye].keys());
        query_field = query_field[0];
        # print("Type  : %s" %query_tpye)
        # print("Field : %s" %query_field)
        index = 0;
        for item in match_body:
             # print("-------------------------")
            item_type = list(item.keys())[0]
            if item_type == query_tpye:
                item_field = list(item[item_type].keys())[0];
                # print("item field  : %s" %item_field)
                # print("query field : %s" %query_field)
                if item_field == query_field:
                    match_body[index] = query;
                    inflag = 1;
            index +=1;
    if inflag == 0:
        match_body.append(query);

#------------Flag에 따라 스코어를 올려주거나, 필터쿼리를 추가합니다-----------
def DoFlag(flag, body, query):

    if flag == 1 :
        PutFilter(body,query);
    elif isInQuery(body,query) == -1 :
        body['min_score'] -= 1

    return;

#------------특수물건 필터가 있다면, 해당 필터쿼리를 만들어서 리턴합니다.-----------
def GetSpecial(set_list):

    return_query = []
    _score = 0
    today = datetime.datetime.now().strftime("%Y.%m.%d")
    # print("Today  : %s" % today)
    for item in set_list:

        if item == 'todaynew':
            temp = {
                "regexp" : {
                    "MulGeonSanSeJoHui.GyeongMaeGaeSil" :{
                        'value' : today,
                        'boost' : 1
                    }
                } # 우선적으로 경매 개시일을 기준으로 공고일을 잡음
            }
            _score += 1
            return_query.append(temp)

        if item == 'resale':
            temp = {
                "regexp" : {
                    "GilNaeYeok.GilNaeYeok.GilGyeolGwa": {
                        "value" : "미납",
                        "boost" : 1
                    }
                }
            }
            _score += 1
            return_query.append(temp)

        if item == 'halfsale':
            temp = {
                "filter": {
                    "script": {
                        "script": {
                            "source": "doc['Sort_min_price'].value != -1 && (doc['Sort_min_price'].value*2 <= doc['Sort_appraisal_price'].value ) ",
                            "lang": "painless"}
                    }
                }
            }
            return_query.append(temp)

        if item == 'afteroneyear':
            target = today.split('.')
            t_year = int(target[0]) -1
            # 일년전 같은 달 매물들을 모두 출력
            target = str(t_year) + "." + target[1] + "." + "[0-9]*"
            # print(target)
            temp = {
                "regexp": {
                    "SaGeonNaeYeok.SaGeonGiBonNaeYeok.GaeSiGyeolJyeonIlJa" : {
                        "value" : target,
                        "boost" : 1
                    }
                }
            }
            _score += 1
            return_query.append(temp)

        # 감정평가서, 매각물건명세서에 위반여부 기입됨, 현재 정보가 없음
        # if item == 'violation':

        if item == 'setting':
            target = '전세권자|임차인'
            temp = {
                "regexp": {
                    "SaGeonNaeYeok.DangSaJaNaeYeok.DangSaJaGuBun": {
                        "value" : target,
                        "boost" : 1
                    }
                }
            }
            _score += 1
            return_query.append(temp)


    return_query.append(_score)
    # print(return_query)
    return return_query

#------------특수물건 필터를 초기화 해줍니다.-----------
def InitSpecial(body):
    # print("------Init--------------------------")
    match_body = body['query']['bool']
    match_oper = 'must'
    if 'should' in match_body:
        match_oper = 'should'
    match_body = match_body[match_oper]
    set_list = [
        "MulGeonSanSeJoHui.GyeongMaeGaeSil",
        "GilNaeYeok.GilNaeYeok.GilGyeolGwa",
        "SaGeonNaeYeok.SaGeonGiBonNaeYeok.GaeSiGyeolJyeonIlJa",
        "SaGeonNaeYeok.DangSaJaNaeYeok.DangSaJaGuBun"
    ]

    for index,item in enumerate(match_body):
        q_type = list(item.keys())[0]
        q_filed = list(item[q_type].keys())[0]
        # print(q_type+ ": " + q_filed)
        if q_type == 'regexp' :
            if q_filed in set_list:
                match_body.pop(index)
        # elif q_type == 'filter':
        #     if q_filed in set_list:
        #         match_body.pop(index)
    if 'filter' in body['query']['bool']:
        del body['query']['bool']['filter']

    print(match_body)
    body['query']['bool'][match_oper] = match_body

#------------경매절차 필터를 만들어서 리턴합니다.-----------
def GetProcedure(p_type):
    if p_type == '':
        return;
    elif p_type == 'random':
        p_type = '임의'
    elif p_type == 'force':
        p_type = '강제'
    elif p_type == 'all':
        p_type = ''
    return_query = {
        "regexp": {
            "SaGeonNaeYeok.SaGeonGiBonNaeYeok.SaGeonMyeong": {
                "value": "[가-힣]*" + p_type + "[가-힣]*",
                "boost": 1
            }
        }
    }
    return  return_query

#-------------지번범위 필터를 만들어서 리턴합니다 ------------
# 현재 지번주소가 없는 매물의 경우 나타나지 않습니다.
def GetStreet(s_list):
    # print(s_list)
    # print(type(s_list[0]))
    s_type = s_list[0] # 전체, 일반 , 산
    s_start = s_list[1]
    s_end   = s_list[2]
    s_reg   = '[가-힣]*[동리]+ '
    # print(type(s_type))

    if (s_type == 'all') | (s_type == ''):
        s_reg += '(<' + s_start + '-' + s_end + '>|' + '산<' +s_start +'-' +s_end + '>)'
    if s_type == 'generl':
        s_reg += "<" + s_start + "-" + s_end + ">"
    if s_type == 'moutian':
        s_reg += "산<"+ s_start + "-" + s_end + ">"

    s_query = {
        "regexp": {
            "SaGeonBeonHo.SoJaeJi": {
                "value": s_reg
            }
        }
    }
    return s_query
