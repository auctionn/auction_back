import json
import re
from datetime import date, datetime
from pprint import pprint
from urllib.parse import quote

import requests
from django.http import HttpResponse
from django.shortcuts import render


from elasticsearch import Elasticsearch

from common_util.const_value import ES_URL
# Create your views here.
from price.function import getMeameaQuery, getAddress,  getAddNumber

es = Elasticsearch(ES_URL);


def actual_price(request) :
    req_content = request.GET;
    postId = req_content.get('postId')
    m2_query = req_content.get('m2')
    new_address = req_content.get('address')
    buildnm = req_content.get('buildnm')
    print('---------------------Atucal Price Top--------------------------')
    # print("Post ID : %s" %postId)
    # print("m2_q : %s" %m2_query)
    # print("new add  : %s" %new_address)
    # print("build num  : %s" %buildnm)
    # print("Type : %s" %req_content.get('type'))
    # print("Years: %s" %req_content.get('years'))
    house_size = "59"
    _year = req_content.get('years')
    if(_year == None) | (_year == 'fiveyears'):
        _year = 5
    else:
        _year = 10

    if postId is not '' :
       new_address, buildnm = getAddress(postId)
    else :
        new_address = req_content.get('address')
        buildnm = req_content.get('buildnm')
        new_address +=' ' +  getAddNumber(new_address)
        if new_address is None :
            new_address = ''
        if buildnm is None :
            buildnm = ''
        print(new_address)
        print(buildnm)
    place = new_address.split(' ')
    type_filter = req_content.get('type')
    type_filter = '아파트매매' if type_filter is None else '*' + type_filter + '*'
    m2_query = "*" if m2_query == 'All' else m2_query
    query_type = 'a' if req_content.get('years') is None else req_content.get('years')
    print(type_filter)
    if query_type == 'a':
        _year = 5
    elif query_type == 'b':
        _year = 10
    elif query_type == 'c':
        type_filter = '*'

    meamea_query = getMeameaQuery(new_address= place, type_filter = type_filter, buildnm = buildnm
                   , m2_query = m2_query, house_size = house_size,_month= 0, _year= _year)
    data = es.search(index='meamea', body=meamea_query)

    # print ("-----------------------------")
    # print("Total : %s " %data['hits']['total'])
    # pprint (meamea_query)

    #------------API TEST--------------
    api_key = 'U01TX0FVVEgyMDE5MDMyNTEzNDgwMzEwODYwMDY='



    parsing_data = dict()
    arr = list()

    for value in data['hits']['hits']:
        record = dict()
        record['type']= value['_source']['type']
        record['price']= ''
        p = "([^\s]전월세)"
        mat = re.search(p,record['type'])
        if mat is not None:
            t = '보증금(만원)'
            record['apt_type']= 'a';
            record['deposit']= value['_source'][t];
            record['price']= record['deposit'].replace(",", "")
            record['price']= record['price'].replace(" ","")
            record['price']= int(record['price'])
            # print('0-------------%s'%record['price'])
        else:
            record['apt_type']= 'b'
            t = '거래금액'
            record['price']= value['_source'][t]
            record['price'] = record['price'].replace(",", "")
            record['price'] = record['price'].replace(" ", "")
            record['price']= (int)(record['price'])
            # print('1-------------%s'%record['price'])

        record['sigungu']= value['_source']['시군구']
        record['bunji']= value['_source']['번지']
        record['bonbun']= value['_source']['본번']
        record['bubun']= value['_source']['부번']

        if '단지명' in value['_source'] :
            record['danjinm']= value['_source']['단지명']

        record['m2']= value['_source']['전용면적']
        record['yearsmonth']= int(value['_source']['계약년월'])
        record['day']= value['_source']['계약일']
        record['floor']= value['_source']['층']
        record['buildyears']= value['_source']['건축년도']
        record['doronm']= value['_source']['도로명']
        record['datadate']= value['_source']['date']

        arr.append(record)

    parsing_data['records'] = arr;

    status = len(arr)
    if status > 0:
        status = "200"
        message = "조회되었습니다."
    else:
        status = "500"
        message = "조회 실패했습니다."

    rst = {"status": status, "message": message, "data": parsing_data}
    rst = json.dumps(rst)

    print('---------------------Atucal Price Bottom--------------------------')

    response = HttpResponse(rst)
    response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    return response;


def avg_price(request):

    req_content = request.GET;
    postId = req_content.get('postId')
    m2_query = req_content.get('m2')
    new_address = ''
    buildnm = ''
    house_size = "59"
    target_month = 9
    print('---------------------Avg Price Top--------------------------')
    # print("m2_query     :     %s " %m2_query)
    # print("type Fileter :     %s " %req_content.get('type'))
    if postId is not '' :
      new_address, buildnm = getAddress(postId)
    else :
        new_address = req_content.get('address')
        buildnm = req_content.get('buildnm')
        new_address += ' ' + getAddNumber(new_address)
        if new_address is None :
            new_address = ''
        if buildnm is None :
            buildnm = ''

    place = new_address.split(' ')
    type_filter = req_content.get('type')
    type_filter = '아파트매매' if type_filter is None else  '*'+type_filter+ '*'
    m2_query = "*" if m2_query == 'All' else m2_query

    meamea_query = getMeameaQuery(new_address=place, type_filter=type_filter, buildnm=buildnm
                                 , m2_query=m2_query, house_size=house_size, _month= target_month, _year=0)
    data = es.search(index='meamea', body=meamea_query)
    # print("Total : %s" %data['hits']['total'])
    # pprint(meamea_query)

    parsing_data = dict()

    six_sum = 0;
    six_idx = 0;
    one_sum = 0;
    one_idx = 0;
    isTrue = 0;

    one_modify_type = datetime.now().month - 1
    if one_modify_type < 1:
        one_modify_type = str(datetime.now().year - 1) + str(one_modify_type + 12)
    else:
        if one_modify_type < 10:
            one_modify_type = str(datetime.now().year) + '0' + str(datetime.now().month - 1)
        else:
            one_modify_type = str(datetime.now().year) + str(datetime.now().month - 1)

    # print("--------AVG----------")
    # pprint(meamea_query)
    # print("Total : %s " %data['hits']['hits'][0])

    record = dict()
    for value in data['hits']['hits'] :
        # print('-----------------------------------------')
        # print(value['_source'])
        # print('-----------------------------------------')
        if int(value['_source']['계약년월']) >= int(one_modify_type) :
            isTrue = 1
            record['dd']= value['_source']['계약년월']
            # print("DD : -------------- %s" %record['dd'])

        record['type'] = value['_source']['type']
        record['price'] = ''
        p = "([^\s]전월세)"
        mat = re.search(p,record['type'])

        if mat is not None :
            t = '보증금(만원)'
            record['apt_type']= 'a';
            record['deposit']= value['_source'][t]
            record['price']= record['deposit'].replace(",","")
            record['price']= record['price'].replace(" ","")
            six_sum += (int)(record['price']);
            six_idx += 1

            if isTrue:
                one_sum += int(record['price'])
                one_idx += 1
                isTrue = 0
        else:
            t = '거래금액'
            record['apt_type'] = 'b';
            record['price'] = value['_source'][t]
            record['price'] = record['price'].replace(",", "")
            record['price'] = record['price'].replace(" ", "")
            six_sum += (int)(record['price']);
            six_idx += 1
            if isTrue:
                one_sum += int(record['price'])
                one_idx += 1
                isTrue = 0


    if one_idx != 0 :
        one_avg = (int)(one_sum / one_idx)
    else:
        one_avg = 'none'
    if six_idx != 0:
        six_avg = (int)(six_sum / six_idx)
    else:
        six_avg = 'none'
    record['sixsum'] = six_sum;
    record['onesum'] = one_sum;
    record['sixavg'] = six_avg;
    record['oneavg'] = one_avg;
    address = new_address.split(" ")

    # --------네이버 평면도 url 가져오기 ----------------------------
    if buildnm != "":
        naver_query = address[0] +" "+ address[1]+ " " + buildnm
        # print("Naver Query : %s" %naver_query)
        url = 'https://land.naver.com/search/search.nhn?query=' + quote(naver_query)
        res = requests.get(url)
        p = '(rletNo[=]+)(0|[1-9][0-9]*)'
        mat = re.search(p, res.text)
        if mat is not None:
            record['naver_ground_url'] = "http://land.naver.com/article/groundPlan.nhn?" + str(mat.group())
            # print("mat ----- : %s" % record['naver_ground_url'])
        # print("Url : %s" % url)

    parsing_data['records'] = record
    status = len(record)
    if status > 0:
        status = "200"
        message = "조회되었습니다."
    else:
        status = "500"
        message = "조회 실패했습니다."

    rst = {"status": status, "message": message, "data": parsing_data}
    rst = json.dumps(rst)

    print('---------------------Avg Price Bottom--------------------------')
    response = HttpResponse(rst)
    response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    return response;
