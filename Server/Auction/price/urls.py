# coding=utf-8
from django.urls import path
from . import views

urlpatterns = [
    path('actual_price', views.actual_price, name = 'actual_price'),
    path('avg_price', views.avg_price, name = 'avg_price'),
]
