import re
from datetime import datetime
from pprint import pprint

from elasticsearch import Elasticsearch
from common_util.const_value import ES_URL

es = Elasticsearch(ES_URL);

#----------사건의 주소와, 단지명을 리턴합니다----------------------
def getAddress(postId):

    data = es.get(index='court_en', doc_type='_doc', id=postId)
    address = data['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0]['SoJaeJi']

    building_query = {
        "size": 1,
        "query": {
            "bool": {
                "should": [
                    {"match": {'newPlatPlc': address}},
                    {"match": {'platPlc': address}}
                ]
            }
        }
    }

    building_data = es.search(index='building', body=building_query)
    value = building_data['hits']['hits'][0]
    addr = value['_source']['newPlatPlc']
    buildnm = value['_source']['bldNm']

    addr += " "+ value['_source']['platPlc'].split(' ')[-1].replace('번지','')
    # print("In getA : %s" %value['_source']['platPlc'])
    return addr, buildnm

#----------번지수를 리턴합니다.----------------------
def getAddNumber(new_address):
    building_query = {
        "_source": ['platPlc','newPlatPlc'],
        "size": 11,
        "query": {
            "bool": {
                "should": [
                    {"match": {'newPlatPlc': new_address}}
                ]
            }
        }
    }
    building_data = es.search(index='building', body=building_query)
    add_num = building_data['hits']['hits'][0]['_source']['platPlc']
    print(add_num)
    return add_num.split(' ')[-1].replace('번지','')

#-----------m_list에 맞는 매매 쿼리를 만들어서 리턴합니다------------
def getMeameaQuery(**m_list):

    place = m_list['new_address']
    type_filter = m_list['type_filter']
    buildnm = m_list['buildnm'].split(" ")[0]
    m2_query = m_list['m2_query']
    house_size = m_list['house_size']
    _month = m_list['_month'] if m_list['_month'] is not None else 0
    _year = m_list['_year']

    sigungu = ' '
    new_add = ' '

    # print("In getMea : %s %s" %(place,buildnm))

    for i in range(0, len(place)):
        last = place[-1]
        p = "([^\s]길|[^\s]로)"
        mat = re.search(p, place[i])
        if mat is not None:
            new_add = place[i]
            break;
        else:
            sigungu += place[i] + ' '

    if type_filter is None:
        type_filter = '아파트매매'

    sigungu_type = '시군구'
    doromn_type = '도로명'

    date_type_str = "계약년월"

    current_year = str(datetime.now().year)
    modify_year = str(int(datetime.now().year)- _year)

    current_month = datetime.now().month
    modify_month = datetime.now().month - _month
    if modify_month < 0:
        modify_month += 12
        modify_year = str(int(modify_year) - 1)

    if current_month < 10:
        current_day = current_year + '0' + str(current_month)
    else:
        current_day = current_year + str(current_month)

    if modify_month < 10:
        modify_day = modify_year + '0' + str(modify_month)
    else:
        modify_day = modify_year + str(modify_month)


    meamea_query = {
        "size": 150,
        "from": 0,
        "sort": [{'_id': {"order": 'desc'}}],
        "query": {
            "bool": {
                "must": [
                    {"match": {sigungu_type: {"query": sigungu, "operator": "and"}}},
                    {"match": {doromn_type: new_add}},
                    {"wildcard": {"type": type_filter}},
                    # {"wildcard": {m2_str: m2_query}},
                    {"range": {date_type_str: {"from": modify_day, "to": current_day}}},
                    # {"wildcard": {"전용면적": house_size+".*"}},
                    # {"wildcard": {"단지명": buildnm +"*"}},
                    {"match": {"번지" : place[-1]}}
                ]
            }
        }
    }

    return meamea_query