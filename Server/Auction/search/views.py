import json
from datetime import datetime, date
from pprint import pprint

from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from elasticsearch import Elasticsearch

from common_util.const_value import ES_URL
# Create your views here.
from common_util.util import isRefer

es = Elasticsearch(ES_URL);

def autocomplete(request) :
    req_content = request.GET;
    keyword = req_content.get('keyword')
    # print("keyword : %s" %keyword )
    # keyword = "사상"
    region = list()
    court = list()
    court_id = list()
    id_keyword = keyword

    #------------------------지역 및 법원 검색 -----------------------
    query = {
        "size" : 5,
         "query": {
           "bool": {
             "must": [
               {"match": {"keyword_id":
                              {"query" : keyword,
                               # "analyzer": "standard"
                               }}}
             ]
           }
         }
    }
    rst = es.search(index='autocomplete', doc_type='_doc', body= query)
    rst = rst['hits']['hits']
    for v in rst :
        if v['_source']['category'] == 'region':
            region.append(v['_source']['keyword_id'])
        if v['_source']['category'] == 'court':
            court.append(v['_source']['keyword_id'])
     #-----------------------사건 번호검색 --------------------------
    if '타경' in keyword :
        id_keyword = keyword + "[0-9]*"
    else :
        id_keyword = keyword + "타경[0-9]*"
    query = {
        "_source" : "SaGeonBeonHo.SaGeonBeonHo",
        "size" : 5,
        "query": {
            "regexp": {
              "SaGeonBeonHo.SaGeonBeonHo": {
                "value" : id_keyword
                }
            }
        }
    }
    rst = es.search(index='court_en', doc_type='_doc', body= query)
    rst = rst['hits']['hits']
    for v in rst:
        court_id.append(v['_source']['SaGeonBeonHo']['SaGeonBeonHo'])
        print("Id : %s" %court_id )

    if len(region) == 0:
        region.append('')
    if len(court) == 0 :
        court.append('')
    if len(court_id) == 0:
        court_id.append('')

    data = dict();
    data['region'] = region
    data['court'] = court
    data['court_id'] = court_id
    send_data = {"data": data}

    print(data)

    data = json.dumps(data)
    # Cross Domain 설정
    response = HttpResponse(data)
    response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    return response;




def search(request):
    #---------------Top------------------
    print("-------------Search------------------")
    # print("-------------%s------------------" %request.GET)
    req_content = request.GET;
    # keyword = "2015 사상"
    keyword = req_content.get('keyword')
    id_keyword = ''
    year_keyword = ''
    address_keyword = ''
    court_keyword = ''
    object_keyword = ''
    MokRokGuBun = '(집합건물|건물|토지)'
    MulGeonYongDo = ['아파트', '단독주택', '연립주택', '빌라', '상가', '근린시설', '오피스텔', '사무실']
    min_score = 13;
    court_score = 0.1; # 법원검색이 아닐 경우에는 가산점만 부여합니다.
    id_scroe = 1
    arr = list()
    currentPage = req_content.get('currentPage')
    recordPerPage = req_content.get('$recordPerPage')

    parsing_data = dict()  # 최종적으로 보낼 데이터 딕셔너리

    if currentPage is None:
        currentPage = '1'
    if recordPerPage is None:
        recordPerPage = '24'  # 현재 페이지 수

    print("This page : %s" % currentPage)
    print(recordPerPage)
    offset = (int(currentPage) - 1) * int(recordPerPage)


    print("Before keyword : %s" %keyword)
    keyword = keyword.split(" ")
    print("After keyword : %s" %keyword)
    for v in keyword :
        if '법원' in v :  # 법원검색일 경우, 지역매칭을 막기위해 스코어를 1점 올려줍니다.
            min_score += 1;
            court_score += 2;
        if '타경' in v : # 전체 사건번호 검색
            id_keyword += "(" + v + ")|"
            year_keyword += "(" + v + ')|'
            id_scroe = 2;
        elif v.isdigit() : # 키워드가 숫자이면 사건번호에 매칭합니다. (해당년도 검색 및 사건번호 일부 검색)
            id_keyword += "([0-9]*타경" + v + ")|"
            year_keyword += "(" + v + '타경[0-9]*)|'
            # id_scroe = 2;
        elif v not in MulGeonYongDo : # 숫자가 아니고, 물건용도가 아니라면 주소,법원에 매칭
            address_keyword += v + "[가-힣]* "
            court_keyword += "(" + v + "[가-힣]*(법원)*)|"
        else :
            object_keyword += "(" + v + ")|"

    address_keyword = address_keyword.rstrip() # 마지막 공백을 지워줍니다.
    address_keyword = "(" + address_keyword +')'

    # 마지막 한 문자( | )를 제거합니다.
    id_keyword = id_keyword[:-1]
    year_keyword = year_keyword[:-1]
    court_keyword = court_keyword[:-1]
    object_keyword = object_keyword[:-1]

    # 해당 키워드가 없을 경우 전체검색을 설정하여 매칭을 허용합니다..
    if object_keyword == '':
        object_keyword = '[가-힣]*'
    if id_keyword == '':
        id_keyword = '[0-9]*타경[0-9]*'
        year_keyword = '[0-9]*타경[0-9]*'

    # if len(keyword) > 1:
    query = {
        # 'sort': [{'Sort_id': {'order': 'desc'}}],
        "min_score": min_score,
        "from": offset,
        "size": recordPerPage,
        "query": {
          "bool": {
            "should": [
                # {"regexp": {"SaGeonBeonHo.SaGeonBeonHo": {"value": id_keyword, "boost": 1}}},
                {"regexp": {"SaGeonBeonHo.SaGeonBeonHo": {"value": year_keyword, "boost": id_scroe}}},
                {"regexp": {"SaGeonBeonHo.SoJaeJi": {"value": address_keyword, "boost": 1}}},
                {"regexp": {"SaGeonNaeYeok.MulGeonNaeYeok.MokRokGuBun": {"value": MokRokGuBun, "boost": 10}}},
                {"regexp": {"DamDangBubWon": {"value": court_keyword, "boost": court_score}}},
                {"regexp": {"SaGeonNaeYeok.MulGeonNaeYeok.MulGeonYongDo": {"value": object_keyword, "boost": 1}}}
            ]
          }
        }
        }

    data = es.search(index="court_en", body=query)
    # data = es.search(index="court_en_backup", body=query)

    data_hits = data['hits']['hits']

    # ------------ parsing data set -----------------------------
    parsing_data['foundRecord'] = data['hits']['total']
    parsing_data['currentPage'] = currentPage
    parsing_data['keyword'] = keyword

    print("Total : %s" % parsing_data['foundRecord'])
    # print("Query : %s" % query)
    count = 0;

    for v in data_hits:
        # print(v['_id'])
        # count =  count+1
        # print("-------------------count : %s------------------------" % count)
        # print(v['_source']['MulGeonSanSeJoHui']['MuGunSaJin']['SaJinJeongBo'])
        # print("-------------------------------------------")

        # ----------- 정렬을 위한 사건번호 추출 --------------
        sort_id = v['_id'].split('-')
        year = int(sort_id[0]) * 1000000
        sageon_id = int(sort_id[1])
        sort_id = int(year + sageon_id)

        # --------- 대표이미지 추출 ---------------------------
        sajin = ''

        # ------- 감정평가액, 최저가격 추출-----------------------
        # print(v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok'])
        price = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['GamJeongPyeongGaAek']
        price = price.replace("원", "")
        price = price.split(" ")
        # print("-------------------------------------------")
        if (len(price) > 2):
            appraisal_price = price[0]
            appraisal_price_min = price[2]
            price[2] = price[2].replace(",", "")
            price[0] = price[0].replace(",", "")
            # sort_price = int(price[0])
            # sort_min_price = int(price[2])
            appraisal_percent = int(int(price[2]) * 100 / int(price[0]))
        else:
            sort_price = ''
            sort_min_price = int(price[0].replace(",", ""))
            appraisal_price = price[0]
            appraisal_price_min = ''
            appraisal_percent = ''

        # ------- 기일 D-day 추출-----------------------
        if 'GilJeonBo' in v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']:
            gildata = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['GilJeonBo']
            d_gildata = gildata.split('.')
            # print("gil-data : %s" %d_gildata)

            if len(d_gildata) > 2:
                targetDate = datetime.strptime(gildata, "%Y.%m.%d").date()
                toDate = date.today()
                d_day = toDate - targetDate
                d_day = d_day.days
            else:
                d_day = ''
        else:
            # print("gil-data : %s" %v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok'])
            d_day = ''
            gildata = ''

        # ------- 물건상태 추출-----------------------
        status = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['MulGeonSangTae']
        status = status.replace(" ", "")
        status_arr = status.split("-")

        if len(status_arr) <= 1:
            status = status_arr
        else:
            status = status_arr[len(status_arr) - 1]

        if "Sort_appraisal_price" not in v['_source']:
            v['_source']['Sort_appraisal_price'] = ''

        if "Sort_min_price" not in v['_source']:
            v['_source']['Sort_min_price'] = ''

        if "Sort_min_price" not in v['_source']:
            v['_source']['Sort_min_price'] = ''

        record = dict()
        record['ID'] = v['_id']
        record['sort_id'] = sort_id
        record['record_id'] = v['_source']['SaGeonNaeYeok']['SaGeonGiBonNaeYeok']['SaGeonBunHo']
        record['record_title'] = record['record_id']
        record['record_type'] = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['MulGeonYongDo']
        record['record_address'] = v['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0][
            'SoJaeJi']  # 왜 0번째 내역을 가져오지?
        record['record_thumb'] = sajin
        record['record_gamjeongayaek'] = v['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['GamJeongPyeongGaAek']
        record['record_gil'] = v['_source']['GilNaeYeok']['GilNaeYeok']
        record['record_price'] = price
        record['record_Appraisal_price'] = appraisal_price
        record['sort_appraisal_price'] = v['_source']['Sort_appraisal_price']
        record['record_Appraisal_min_price'] = appraisal_price_min
        record['sort_min_price'] = v['_source']['Sort_min_price']
        record['record_Appraisal_price_percent'] = appraisal_percent
        record['gildata'] = gildata
        record['record_gil_dday'] = d_day
        record['lat'] = v['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0]['y']
        record['lng'] = v['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0]['x']
        record['record_MulGeonStatus'] = status
        #---------Debuging Data-----------------
        record['score'] = v['_score']
        record['court'] = v['_source']['DamDangBubWon']
        arr.append(record)

    # ------------ Debugging-------------------------
    # for v in arr:
    #     print("-------------------------------------------")
    #     print("ID      : %s" %v['sort_id'])
    #     print("Address : %s" %v['record_address'])
    #     print("Court   : %s" %v['court'])
    #     print("Category: %s" %v['record_type'])
    #     print("Score   : %s" %v['score'])


    # parsing_data['records'] = arr
    parsing_data['query'] = query;
    parsing_data['searchFlag'] = 'true';

    d = isRefer(request.get_host())


    rst = {"status": d['status'], "message": d['message'], "data": parsing_data, "query": query, }
    # rst = {"status": d['status'], "message": d['message'], "query": query, }
    rst = json.dumps(rst)
    # print(query)
    print("HTTP_HOST : %s" % request.get_host())
    # print(rst)
    # print(rst)

    # Cross Domain 설정
    # Cross Domain 설정
    response = HttpResponse(rst)
    response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"


    return response;