from datetime import date, datetime
import re
from pprint import pprint
from urllib import request

import certifi
import requests
from elasticsearch import Elasticsearch

from common_util.const_value import ES_URL

from common_util.const_value import DOMAIN
from requests_aws4auth import AWS4Auth

from common_util.const_value import ES_URL

from common_util.const_value import DOMAIN

# awsauth = AWS4Auth(YOUR_ACCESS_KEY, YOUR_SECRET_KEY, REGION, 'es')

# ES = Elasticsearch(ES_URL,
#                    http_auth=awsauth,
#                    use_ssl=True,
#                    verify_certs=True,
#                    connection_class=RequestsHttpConnection
#                    );

ES = Elasticsearch([ES_URL], use_ssl=True, ca_certs=certifi.where())
# ES = Elasticsearch(ES_URL)
state_query = {

};

def getDomain() :
    return DOMAIN

def isRefer(host):
    if getDomain() in host :
        return {"status" : "true", "message" : "Ok"}
    else:
        return {"status": "false", "message": "Sorry"}


# #------------ 필터 flag값을 설정해주고, 필터범위를 리턴합니다. -------------
# def SetFlag(fill_list, initStart, initEnd):
#
#     if fill_list[0] == '-1': # 초기화
#         fill_list = [initStart,initEnd,1]
#     elif fill_list[0] == '' : #필터없음
#         fill_list = [initStart,initEnd,0]
#     else :
#         fill_list.append(1)
#     return fill_list;
#
# #------------ body 안에 query가 있는지 확인합니다.--------------------
# def isInQuery(body,query) :
#     match_body = body['query']['bool']
#     match_oper = 'must'
#     if 'should' in match_body:
#         match_oper = 'should'
#     match_body = match_body[match_oper]
#
#     query_tpye = list(query.keys());
#     query_tpye = query_tpye[0];
#     inflag = 0;
#
#     if query_tpye != 'match_all':
#         query_field = list(query[query_tpye].keys());
#         query_field = query_field[0];
#         # print("Type  : %s" %query_tpye)
#         # print("Field : %s" %query_field)
#         index = False;
#         for item in match_body:
#             # print("-------------------------")
#             item_type = list(item.keys())[0]
#             if item_type == query_tpye:
#                 item_field = list(item[item_type].keys())[0];
#                 # print("item field  : %s" %item_field)
#                 # print("query field : %s" %query_field)
#                 if item_field == query_field:
#                     inflag = True;
#         return inflag
#
# #------------프론트에서 넘어온 쿼리에 필터쿼리를 추가합니다.-----------------
# def PutFilter(body, query) :
#     #     print("Parameter must be dict type");
#     match_body = body['query']['bool']
#     match_oper = 'must'
#     if 'should' in match_body :
#         match_oper = 'should'
#     match_body = match_body[match_oper]
#
#     query_tpye = list(query.keys());
#     query_tpye = query_tpye[0];
#     inflag = 0;
#
#     if query_tpye != 'match_all' :
#         query_field = list(query[query_tpye].keys());
#         query_field = query_field[0];
#         # print("Type  : %s" %query_tpye)
#         # print("Field : %s" %query_field)
#         index = 0;
#         for item in match_body:
#              # print("-------------------------")
#             item_type = list(item.keys())[0]
#             if item_type == query_tpye:
#                 item_field = list(item[item_type].keys())[0];
#                 # print("item field  : %s" %item_field)
#                 # print("query field : %s" %query_field)
#                 if item_field == query_field:
#                     match_body[index] = query;
#                     inflag = 1;
#             index +=1;
#     if inflag == 0:
#         match_body.append(query);
#
# #------------Flag에 따라 스코어를 올려주거나, 필터쿼리를 추가합니다-----------
# def DoFlag(flag, body, query) :
#
#     if flag == 1 :
#         PutFilter(body,query);
#     elif isInQuery(body,query) == False :
#         body['min_score'] -= 1
#
#     return;

def getAvgPrice(new_address , buildnm, m2_query):
    place = new_address.split(' ')
    sigungu = ' '
    new_add = ' '

    # print("Place is %s" %place)

    for i in range(0, len(place)):
        last = place[-1]
        p = "([^\s]길|[^\s]로)"
        mat = re.search(p, place[i])
        if mat is not None:
            new_add = place[i]
            # print("new_add is %s" % new_add)
            break;
        else:
            sigungu += place[i] + ' '
            # print("sigungu is %s" % sigungu)

    apt_type = '*'
    # type_filter = '*' + type_filter + '*'
    # if type_filter is None:
    type_filter = '*전월세*'

    subject = buildnm

    # print("Subject : %s" %subject)

    if (subject is not None):
        apt_type = subject + '*'

    target_month = 6

    sigungu_type = '시군구'
    doromn_type = '도로명'

    date_type_str = "계약년월"
    apt_type_str = "단지명"
    m2_str = "전용면적"

    if m2_query == 'All':
        m2_query = '*'

    if int(datetime.now().month) < 10:
        current_day = str(datetime.now().year) + '0' + str(datetime.now().month)
        if int(datetime.now().month) - target_month < -2:
            modify_day = str(datetime.now().year - 1) + '0' + str(int(datetime.now().month) - target_month + 12)
        else:
            modify_day = str(datetime.now().year - 1) + str(int(datetime.now().month) - target_month + 12)
    else:
        current_day = str(datetime.now().year) + str(datetime.now().month)
        modify_day = str(datetime.now().year) + str(int(datetime.now().month) - target_month)

    # print(" current day : ----------------%s-------------"%current_day)
    if int(datetime.now().month) - 1 == 0:
        one_modify_type = str(datetime.now().year - 1)
        one_modify_type += '12'
    else:
        one_modify_type = int(current_day) - 1;
    # print('modeify_day : %s' %one_modify_type)

    meamea_query = {
        "size": 150,
        "from": 0,
        "sort": [{'_id': {"order": 'desc'}}],
        "query": {
            "bool": {
                "must": [
                    {"match": {sigungu_type: {"query": sigungu, "operator": "and"}}},
                    {"match": {doromn_type: new_add}},
                    {"wildcard": {"type": type_filter}},
                    {"wildcard": {m2_str: m2_query}},
                    {"range": {date_type_str: {"from": modify_day, "to": current_day}}},
                ]
            }
        }
    }

    data = ES.search(index='meamea', body=meamea_query)

    parsing_data = dict()
    arr = list()

    six_sum = 0;
    six_avg = 0;
    six_idx = 0;

    one_sum = 0;
    one_avg = 0;
    one_idx = 0;
    isTrue = 0;
    type = 'a';

    # pprint(meamea_query)
    # print("Total : %s " % data['hits']['total'])

    record = dict()
    for value in data['hits']['hits']:
        # print('-----------------------------------------')
        # print(value['_source'])
        # print('-----------------------------------------')
        if int(value['_source']['계약년월']) >= int(one_modify_type):
            isTrue = 1
            record['dd'] = value['_source']['계약년월']
            # print("DD : -------------- %s" %record['dd'])

        record['type'] = value['_source']['type']
        record['price'] = ''
        p = "([^\s]전월세)"
        mat = re.search(p, record['type'])

        if mat is not None:
            t = '보증금(만원)'
            record['apt_type'] = 'a';
            record['deposit'] = value['_source'][t]
            record['price'] = record['deposit'].replace(",", "")
            record['price'] = record['price'].replace(" ", "")
            six_sum += (int)(record['price']);
            six_idx += 1

            if isTrue:
                one_sum += int(record['price'])
                one_idx += 1
                isTrue = 0
        else:
            t = '거래금액'
            record['apt_type'] = 'b';
            record['price'] = value['_source'][t]
            record['price'] = record['price'].replace((",", ""))
            record['price'] = record['price'].replace(" ", "")
            six_sum += (int)(record['price']);
            six_idx += 1
            if isTrue:
                one_sum += int(record['price'])
                one_idx += 1
                isTrue = 0

    if one_idx != 0:
        one_avg = (int)(one_sum / one_idx)
    else:
        one_avg = 'none'
    if six_idx != 0:
        six_avg = (int)(six_sum / six_idx)
    else:
        six_avg = 'none'
    record['sixsum'] = six_sum;
    record['onesum'] = one_sum;
    record['sixavg'] = six_avg;
    record['oneavg'] = one_avg;

    return record['sixavg'];
