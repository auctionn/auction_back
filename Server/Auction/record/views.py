import re
from datetime import date, datetime
from pprint import pprint
from urllib.parse import quote

from django.shortcuts import render
import json
from django.http import HttpResponse
from django.shortcuts import render
import requests

# Create your views here.
from elasticsearch import Elasticsearch

from common_util.const_value import ES_URL
# Create your views here.

es = Elasticsearch(ES_URL);



def record(request):
    print("------------------Record Top------------------------------")
    req_content = request.GET;
    postId = req_content.get('postId')
    print("Post ID : %s" %postId)
    if postId is None :
        postId = 261092145

    index = 'court_en'

    data = es.get(index=index, doc_type='_doc', id=postId)

    #--------------------주소 추출---------------------------------
    record_address = data['_source']['SaGeonBeonHo']['SoJaeJi']
    record_address =record_address.split(',')

    #--------------------사진 추출---------------------------------
    sajin_arr = list()

    for item in data['_source']['MulGeonSanSeJoHui']['MuGunSaJin']['SaJinJeongBo']:
        sajin_ex = item.split('jpg?')
        sajin = sajin_ex[0] + 'jpg'
        sajin_arr.append(sajin)

    sajin = ''

    # 데이터 null처리
    if len(data['_source']['MulGeonSanSeJoHui']['MuGunSaJin']['SaJinJeongBo']) > 0:
        sajin_ex = data['_source']['MulGeonSanSeJoHui']['MuGunSaJin']['SaJinJeongBo'][0].split('jpg?')
        sajin = sajin_ex[0] + 'jpg'

    # ------- 가격 추출 -------------------------------
    # ------- Front에서 해도 되지 않을까?-----------------
    price = data['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['GamJeongPyeongGaAek']
    price = price.replace('원', '')
    price = price.split(' ')
    if (len(price) > 2):
        appraisal_price = price[0]
        appraisal_price_min = price[2]
        price[2] = price[2].replace(",", "")
        price[0] = price[0].replace(",", "")
        appraisal_percent = int(int(price[2]) * 100 / int(price[0]))
    else:
        appraisal_price = price[0]
        appraisal_price_min = ''
        appraisal_percent = ''

    record = dict()

    record['ID'] = postId
    record['ta_ID'] = data['_source']['SaGeonNaeYeok']['SaGeonGiBonNaeYeok']['SaGeonBunHo']
    record['title'] = record['ta_ID'].replace('타경', '-')
    record['detail_address'] = data['_source']['SaGeonBeonHo']['SoJaeJi']
    record['address'] = record_address[0]

    # null 처리
    gam = ''
    if 'GamJeongPyeongGaSeo' in data['_source']:
        if data['_source']['GamJeongPyeongGaSeo'][0] is not None :
            gam_ex = data['_source']['GamJeongPyeongGaSeo'][0].split("pdf")
            gam = gam_ex[0] + 'pdf'

    record['pyeonggaseo'] = data['_source']['GamJeongPyeongGaSeo']
    print("평가서 : %s" %record['pyeonggaseo'])
    record['gamjeong'] = gam
    record['sa_title'] = data['_source']['SaGeonNaeYeok']['SaGeonGiBonNaeYeok']['SaGeonMyeong']

    record['title_thumb'] = sajin;
    record['thumbs'] = sajin_arr;
    record['start_date'] = data['_source']['SaGeonNaeYeok']['SaGeonGiBonNaeYeok']['GaeSiGyeolJyeonIlJa']
    record['jub_date'] = data['_source']['SaGeonNaeYeok']['SaGeonGiBonNaeYeok']['JeobSuIlJa']

    record['auction_how'] = data['_source']['MulGeonSanSeJoHui']['MulGunGiBonJeongBo']['IpChalBangBub']

    # null 처리
    if 'MulGunBiGo' in  data['_source']['SaGeonNaeYeok']['MulGeonNaeYeok'] :
        record['Bigo'] = data['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['MulGunBiGo']

    record['gildata'] = data['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['GilJeonBo']
    record['current_date'] = str(date.today())
    record['gildata'] = data['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['GilJeonBo'].replace('.','-')

    record['lat']= data['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0]['y']
    record['lng']= data['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0]['x']

    bubwon = data['_source']['SaGeonNaeYeok']['SaGeonGiBonNaeYeok']['DamDangGye'].split('구내')
    record['bubwon']= data['_source']['DamDangBubWon'] + ' '+ bubwon[0]


    for index, user in enumerate(data['_source']['SaGeonNaeYeok']['DangSaJaNaeYeok']):
        record['user'+str(index)+'_type'] = user['DangSaJaGuBun']
        record['user'+str(index)+'_name'] = user['DangSaJaMyeong']

    record['gamjeongayaek']= data['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['GamJeongPyeongGaAek']
    record['gil']= data['_source']['GilNaeYeok']['GilNaeYeok']
    record['price'] = price
    record['Appraisal_price'] = appraisal_price
    record['Appraisal_min_price'] = appraisal_price_min
    record['Appraisal_price_percent'] = appraisal_percent

    type = data['_source']['SaGeonNaeYeok']['MulGeonNaeYeok']['MulGeonYongDo']

    keyword = record['address']
    #-------작업중-------------
    print("Keyword : %s" %record_address)
    pattern = re.compile("\d+동")
    build_number = ''
    if len(record_address) > 1:
        build_number = pattern.search(record_address[1])
    if (build_number != '') & (build_number is not None):
        build_number = build_number.group().replace('동','')
    else:
        build_number = ''
    if build_number == '':
        dong_q = {"match_all": {}}
    else:
        dong_q = {"wildcard": {'dongNm': build_number+"*"}}

    building_query = {
        "size": 1,
        "query": {
            "bool": {
                # "should": [
                #     {"match" : {'newPlatPlc': {
                #         "query" : keyword,
                #         "operator" : "and"
                #     }}},
                #     {"match": {'platPlc': {
                #         "query": keyword,
                #         "operator": "and"
                #     }}}
                # ]
                "must": [
                    {"multi_match": {
                        "query": keyword,
                        "operator": "and",
                        "fields": ['newPlatPlc', 'platPlc']}
                    },
                   dong_q
                ]
            }
        }
    }
    area_query = {
        "_source": ["archArea", "dongNm", "platArea"],
        "query": {
            "bool": {
                "must": [
                    {"multi_match": {
                        "query": keyword,
                        "operator": "and",
                        "fields": ['newPlatPlc', 'platPlc']}
                    }
                ]
            }
        }
    }

    building_data = es.search(index= 'building',body= building_query)
    area_data = es.search(index='building', body=area_query)['hits'] # 해당 아파트별 면적


    arr = list()
    area = list()
    pprint(building_query)

    # pprint(building_data['hits']['hits'])
    if building_data['hits']['total'] > 0:
        for v in building_data['hits']['hits']:  # 가져온 data 중 하나만 사용,
            record['ID'] = postId
            record['record_id'] = postId
            record['build_title'] = v['_source']['mainPurpsCdNm']
            record['build_address'] = v['_source']['newPlatPlc']
            record['archArea'] = v['_source']['archArea']
            record['platArea'] = v['_source']['platArea']
            record['atchBldArea'] = v['_source']['atchBldArea']
            record['atchBldCnt'] = v['_source']['atchBldCnt']
            record['bcRat'] = v['_source']['bcRat']
            record['bjdongCd'] = v['_source']['bjdongCd']
            record['bldNm'] = v['_source']['bldNm']
            record['block'] = v['_source']['bun']
            record['bylotCnt'] = v['_source']['bylotCnt']
            record['crtnDay'] = v['_source']['crtnDay']
            record['dongNm'] = v['_source']['dongNm']
            record['emgenUseElvtCnt'] = v['_source']['emgenUseElvtCnt']
            record['engrEpi'] = v['_source']['engrEpi']
            record['engrGrade'] = v['_source']['engrGrade']
            record['engrRat'] = v['_source']['engrRat']
            record['etcPurps'] = '아파트'
            record['etcRoof'] = v['_source']['etcRoof']
            record['etcStrct'] = v['_source']['etcStrct']
            record['fmlyCnt'] = v['_source']['fmlyCnt']
            record['gnBldCert'] = v['_source']['gnBldCert']

            record['gnBldGrade'] = v['_source']['gnBldGrade']
            record['grndFlrCnt'] = v['_source']['grndFlrCnt']
            record['heit'] = v['_source']['heit']
            record['hhldCnt'] = '세대정보없음' if v['_source']['hhldCnt'] == 0 else v['_source']['hhldCnt'] + '세대'
            record['hoCnt'] = v['_source']['hoCnt']
            record['indrAutoArea'] = v['_source']['indrAutoArea']
            record['indrAutoUtcnt'] = v['_source']['indrAutoUtcnt']
            record['indrMechArea'] = v['_source']['indrMechArea']
            record['indrMechUtcnt'] = v['_source']['indrMechUtcnt']

            record['itgBldCert'] = v['_source']['itgBldCert']
            record['itgBldGrade'] = v['_source']['itgBldGrade']
            record['ji'] = v['_source']['ji']
            record['lot'] = v['_source']['lot']
            record['mainAtchGbCd'] = v['_source']['mainAtchGbCd']
            record['mainAtchGbCdNm'] = v['_source']['mainAtchGbCdNm']
            record['mainPurpsCd'] = v['_source']['mainPurpsCd']
            record['mainPurpsCdNm'] = v['_source']['mainPurpsCdNm']
            record['mgmBldrgstPk'] = v['_source']['mgmBldrgstPk']
            record['naBjdongCd'] = v['_source']['naBjdongCd']
            record['naMainBun'] = v['_source']['naMainBun']
            record['naSubBun'] = v['_source']['naSubBun']
            record['naUgrndCd'] = v['_source']['naUgrndCd']
            record['newPlatPlc'] = v['_source']['newPlatPlc']
            record['oudrAutoArea'] = v['_source']['oudrAutoArea']
            record['oudrAutoUtcnt'] = v['_source']['oudrAutoUtcnt']
            record['oudrMechArea'] = v['_source']['oudrMechArea']
            record['oudrMechUtcnt'] = v['_source']['oudrMechUtcnt']
            record['platPlc'] = v['_source']['platPlc']
            record['pmsDay'] = v['_source']['pmsDay']
            record['pmsnoGbCd'] = v['_source']['pmsnoGbCd']
            record['pmsnoGbCdNm'] = v['_source']['pmsnoGbCdNm']
            record['pmsnoKikCd'] = v['_source']['pmsnoKikCd']
            record['pmsnoKikCdNm'] = v['_source']['pmsnoKikCdNm']
            record['pmsnoYear'] = v['_source']['pmsnoYear']
            record['regstrGbCd'] = v['_source']['regstrGbCd']
            record['regstrGbCdNm'] = v['_source']['regstrGbCdNm']
            record['regstrKindCd'] = v['_source']['regstrKindCd']
            record['regstrKindCdNm'] = v['_source']['regstrKindCdNm']
            record['rideUseElvtCnt'] = v['_source']['rideUseElvtCnt']
            record['rnum'] = v['_source']['rnum']
            record['roofCd'] = v['_source']['roofCd']
            record['roofCdNm'] = v['_source']['roofCdNm']
            record['sigunguCd'] = v['_source']['sigunguCd']
            record['splotNm'] = v['_source']['splotNm']
            record['stcnsDay'] = v['_source']['stcnsDay']
            record['strctCd'] = v['_source']['strctCd']
            record['strctCdNm'] = v['_source']['strctCdNm']
            record['totArea'] = v['_source']['totArea']
            record['totDongTotArea'] = v['_source']['totDongTotArea']
            record['ugrndFlrCnt'] = v['_source']['ugrndFlrCnt']
            record['stcnsDay'] = v['_source']['stcnsDay']
            record['useAprDay'] = v['_source']['useAprDay']
            record['vlRat'] = v['_source']['vlRat']
            record['vlRatEstmTotArea'] = v['_source']['vlRatEstmTotArea']
            record['lat'] = v['_source']['y']
            record['lng'] = v['_source']['x']
            record['From'] = "Record"


        # record['park_cnt'] = int(record['indrAutoUtcnt']) + int(record['indrMechUtcnt']) + int(record['oudrAutoUtcnt'])
        # + int(record['oudrMechUtcnt'])
        record['park_cnt'] =  10;
        print("Parking : " ,record['park_cnt'])
        record['park_cnt'] = int(record['park_cnt'])
        address =record['address'].split(" ")
        record['naver_ground_url'] = ''
        record['type'] = type
        p = "(오피스텔|아파트|원룸)"
        mat = re.search(p, record['type'])
    #------------- Debug-------------
    # print("Address     : %s" %record['platPlc'])
    # print("New Address : %s" %record['newPlatPlc'])
    # print("Dong Name   : %s" %record['dongNm'])
    # print("Ho   Name   : %s" %record['hoCnt'])
    # print("archArea    : %s" %record['archArea'])
    # print("platArea    : %s" %record['platArea'])

    #--------네이버 평면도 url 가져오기 ----------------------------

        if (record['bldNm'] != "") & (mat is not None):
            naver_query = address[0] + address[1] + record_address[-1]
            print("Naver Query :    %s" %naver_query)
            url = 'https://land.naver.com/search/search.nhn?query=' +quote(naver_query)
            res = requests.get(url)
            p = '(rletNo[=]+)(0|[1-9][0-9]*)'
            mat = re.search(p,res.text)
            print("Url : %s" % url)

        if mat is not None :
            record['naver_ground_url'] = "http://land.naver.com/article/groundPlan.nhn?"+ str(mat.group())


    for item in area_data['hits']:
        temp = dict()
        print("--------------------------")
        pprint("Dong  : %s" %item['_source']['dongNm'])
        pprint("Area  : %s" %item['_source']['archArea'])
        pprint("PArea : %s" %item['_source']['platArea'])
        temp['name'] = item['_source']['dongNm']
        temp['house_size'] = item['_source']['archArea']
        area.append(temp)

    arr.append(record)
    print(area)
    parsing_data = dict()
    parsing_data['records'] = arr
    parsing_data['size_type'] = area

    status = len(arr)
    if status > 0:
        status = "200"
        message = "조회되었습니다."
    else:
        status = "500"
        message = "조회 실패했습니다."

    rst = {"status": status, "message": message, "data": parsing_data}
    print(rst)
    rst = json.dumps(rst)
    print("HTTP_HOST : %s" %request.get_host())
    print(parsing_data['size_type'])
    print("------------------Record Bottom------------------------------")

    # Cross Domain 설정
    response = HttpResponse(rst)
    response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    return response;



