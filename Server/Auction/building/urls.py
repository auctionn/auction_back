# coding=utf-8
from django.urls import path
from . import views


urlpatterns = [
    path('', views.building, name = 'building'),
    path('map_center', views.map_center, name = 'map_center'),
]
