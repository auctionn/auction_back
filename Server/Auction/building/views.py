import json
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from elasticsearch import Elasticsearch

from common_util.const_value import ES_URL

from common_util.util import getAvgPrice

es = Elasticsearch(ES_URL);

def map_center(request) :

    req_content = request.GET;
    postId = req_content.get('postId')
    sw_lat = req_content.get('sw_lat')
    sw_lng = req_content.get('sw_lng')
    ne_lat = req_content.get('ne_lat')
    ne_lng = req_content.get('ne_lng')
    index = 'court_en'

    if sw_lat is None:
       data = es.get(index = index, doc_type= '_doc', id= postId)
    else :
        data = ''

    lat = data['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0]['y']
    lng = data['_source']['SaGeonNaeYeok']['BaeDangYoGuJongGiNaeYeok'][0]['x']

    r = dict()
    r['lat'] = lat;
    r['lng'] = lng;

    arr = list()
    arr.append(r)

    parsing_data = dict()
    parsing_data['records'] = arr;

    status = len(arr)
    if status > 0:
        status = "200"
        message = "조회되었습니다."
    else:
        status = "500"
        message = "조회 실패했습니다."

    rst = {"status": status, "message": message, "data": parsing_data}

    rst = json.dumps(rst)
    # print(rst)

    # Cross Domain 설정
    response = HttpResponse(rst)
    response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"


    return response

def building(request) :

    print('---------------------Building Top--------------------------')
    req_content = request.GET;
    postId = req_content.get('postId')
    sw_lat = req_content.get('sw_lat')
    sw_lng = req_content.get('sw_lng')
    ne_lat = req_content.get('ne_lat')
    ne_lng = req_content.get('ne_lng')
    currentPage = req_content.get('currentPage')
    recordPerPage = req_content.get('$recordPerPage ')
    keyword = req_content.get('keyword')

    # print("Post ID : %s"%postId)
    # print("Request.sw_lat :  %s" % request.GET.get('sw_lat'))
    # print("Request.ne_lat :  %s" % request.GET.get('ne_lat'))
    # print("Request.sw_lng :  %s" % request.GET.get('sw_lng'))
    # print("Request.ne_lng :  %s" % request.GET.get('ne_lng'))

    index = 'building'
    building_x = "x"
    building_y = "y"
    etcPurps_filter = '아파트';

    arr = list()

    parsing_data = dict()  # 최종적으로 보낼 데이터 딕셔너리

    if currentPage is None:
        currentPage = '1'
    if recordPerPage is None:
        recordPerPage = '20'

    offset = (int(currentPage) - 1) * int(recordPerPage)

    query_body = {
        "from": offset,
        "size": recordPerPage,
        "query": {
            "bool": {
                "must": [
                    {"range": {building_x: {"from": sw_lng, "to": ne_lng}}
                     },
                    {"range": {building_y: {"from": sw_lat, "to": ne_lat}}
                     },
                    {"match": {
                        'etcPurps': etcPurps_filter
                    }}
                ]
            }
        }
    }

    data = es.search(
        index=index, body=query_body)
    data_hits = data['hits']['hits']

    for v in data_hits :
        record = dict()
        record['ID'] = postId
        record['record_id'] = postId
        record['build_title'] = v['_source']['mainPurpsCdNm']
        record['build_address'] = v['_source']['newPlatPlc']
        record['archArea'] = v['_source']['archArea']
        record['atchBldArea'] = v['_source']['atchBldArea']
        record['atchBldCnt'] = v['_source']['atchBldCnt']
        record['bcRat'] = v['_source']['bcRat']
        record['bjdongCd'] = v['_source']['bjdongCd']
        record['bldNm'] = v['_source']['bldNm']
        record['block'] = v['_source']['bun']
        record['bylotCnt'] = v['_source']['bylotCnt']
        record['crtnDay'] = v['_source']['crtnDay']
        record['dongNm'] = v['_source']['dongNm']
        record['emgenUseElvtCnt'] = v['_source']['emgenUseElvtCnt']
        record['engrEpi'] = v['_source']['engrEpi']
        record['engrGrade'] = v['_source']['engrGrade']
        record['engrRat'] = v['_source']['engrRat']
        record['etcPurps'] = '아파트'
        record['etcRoof'] = v['_source']['etcRoof']
        record['etcStrct'] = v['_source']['etcStrct']
        record['fmlyCnt'] = v['_source']['fmlyCnt']
        record['gnBldCert'] = v['_source']['gnBldCert']

        record['gnBldGrade'] = v['_source']['gnBldGrade']
        record['grndFlrCnt'] = v['_source']['grndFlrCnt']
        record['heit'] = v['_source']['heit']
        record['hhldCnt'] = '세대정보없음' if v['_source']['hhldCnt'] == 0 else v['_source']['hhldCnt'] + '세대'
        record['hoCnt'] = v['_source']['hoCnt']
        record['indrAutoArea'] = v['_source']['indrAutoArea']
        record['indrAutoUtcnt'] = v['_source']['indrAutoUtcnt']
        record['indrMechArea'] = v['_source']['indrMechArea']
        record['indrMechUtcnt'] = v['_source']['indrMechUtcnt']

        record['itgBldCert'] = v['_source']['itgBldCert']
        record['itgBldGrade'] = v['_source']['itgBldGrade']
        record['ji'] = v['_source']['ji']
        record['lot'] = v['_source']['lot']
        record['mainAtchGbCd'] = v['_source']['mainAtchGbCd']
        record['mainAtchGbCdNm'] = v['_source']['mainAtchGbCdNm']
        record['mainPurpsCd'] = v['_source']['mainPurpsCd']
        record['mainPurpsCdNm'] = v['_source']['mainPurpsCdNm']
        record['mgmBldrgstPk'] = v['_source']['mgmBldrgstPk']
        record['naBjdongCd'] = v['_source']['naBjdongCd']
        record['naMainBun'] = v['_source']['naMainBun']
        record['naSubBun'] = v['_source']['naSubBun']
        record['naUgrndCd'] = v['_source']['naUgrndCd']
        record['newPlatPlc'] = v['_source']['newPlatPlc']
        record['oudrAutoArea'] = v['_source']['oudrAutoArea']
        record['oudrAutoUtcnt'] = v['_source']['oudrAutoUtcnt']
        record['oudrMechArea'] = v['_source']['oudrMechArea']
        record['oudrMechUtcnt'] = v['_source']['oudrMechUtcnt']
        record['platPlc'] = v['_source']['platPlc']
        record['pmsDay'] = v['_source']['pmsDay']
        record['pmsnoGbCd'] = v['_source']['pmsnoGbCd']
        record['pmsnoGbCdNm']	= v['_source']['pmsnoGbCdNm']
        record['pmsnoKikCd']	= v['_source']['pmsnoKikCd']
        record['pmsnoKikCdNm']	= v['_source']['pmsnoKikCdNm']
        record['pmsnoYear']	= v['_source']['pmsnoYear']
        record['regstrGbCd']	= v['_source']['regstrGbCd']
        record['regstrGbCdNm']	= v['_source']['regstrGbCdNm']
        record['regstrKindCd']	= v['_source']['regstrKindCd']
        record['regstrKindCdNm']	= v['_source']['regstrKindCdNm']
        record['rideUseElvtCnt']	= v['_source']['rideUseElvtCnt']
        record['rnum']	= v['_source']['rnum']
        record['roofCd']	= v['_source']['roofCd']
        record['roofCdNm']	= v['_source']['roofCdNm']
        record['sigunguCd']	= v['_source']['sigunguCd']
        record['splotNm']	= v['_source']['splotNm']
        record['stcnsDay']	= v['_source']['stcnsDay']
        record['strctCd']	= v['_source']['strctCd']
        record['strctCdNm']	= v['_source']['strctCdNm']
        record['totArea']	= v['_source']['totArea']
        record['totDongTotArea']	= v['_source']['totDongTotArea']
        record['ugrndFlrCnt']	= v['_source']['ugrndFlrCnt']
        record['stcnsDay']	= v['_source']['stcnsDay']
        record['useAprDay']	= v['_source']['useAprDay']
        record['vlRat']	= v['_source']['vlRat']
        record['vlRatEstmTotArea'] = v['_source']['vlRatEstmTotArea']
        record['lat'] = v['_source']['y']
        record['lng'] = v['_source']['x']
        record['From'] = "Building"
        record['sixAvg'] = getAvgPrice( record['newPlatPlc'],record['bldNm'], 'All');
        # print("Price : %s" %record['sixAvg'])
        arr.append(record)

    parsing_data['records'] = arr

    status = len(arr)
    if status > 0:
        status = "200"
        message = "조회되었습니다."
    else:
        status = "500"
        message = "조회 실패했습니다."

    rst = {"status": status, "message": message, "data": parsing_data}
    rst = json.dumps(rst)

    print('---------------------Building Bottom--------------------------')
    # Cross Domain 설정
    response = HttpResponse(rst)
    response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    return response;





